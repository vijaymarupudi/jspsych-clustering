#!/usr/bin/env bash

npx webpack -p
ssh server << EOF
  mkdir /websites/experiments.vijaymarupudi.com/clustering -p
  mkdir /websites/experiments.vijaymarupudi.com/api -p
EOF
rsync dist/ server:/websites/experiments.vijaymarupudi.com/clustering -aP

rsync serversrc/ server:/websites/experiments.vijaymarupudi.com/api/ -aP

ssh server << EOF
  sudo systemctl restart experiments-flask-server.service
EOF
